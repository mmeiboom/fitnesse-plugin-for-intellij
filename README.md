Notes and lessons
* Do not overwrite lexing of EOF token. The lexer expect "0" to be returned or it will go into an infinite loop.
    
# Todo
* recoverWhile breaks correct table layout, but we do need it to deal with error tokens. Investigate.
* navigate to definition/scenario/fixture
* empty table cells break formatting
* formatting table header
* arguments for !today command
 
# Resources
* http://confluence.jetbrains.com/display/IntelliJIDEA/Custom+Language+Support.
* https://theantlrguy.atlassian.net/wiki/display/~admin/Intellij+plugin+development+notes
* https://code.google.com/p/ide-examples/wiki/IntelliJIdeaPsiCookbook
* http://confluence.jetbrains.com/display/IDEADEV/PluginDevelopment
* http://confluence.jetbrains.com/display/IDEADEV/Developing+Custom+Language+Plugins+for+IntelliJ+IDEA
* http://devnet.jetbrains.com/community/idea/open_api_and_plugin_development
* https://github.com/nicoulaj/idea-markdown
