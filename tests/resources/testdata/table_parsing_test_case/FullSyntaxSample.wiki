!1 Syntax examples

Full page also at
http://www.fitnesse.org/FitNesse.UserGuide.QuickReferenceGuide#VARIABLES

!2 Text formatting
Text containing a word in ''italics'' with double quotes.
Text containing a word in '''bold''' with double quotes.
Text containing a word in '''''bold-italics'''''?
Text containing a --striked-- word.
Text containing a word with some !style_red{additional styling}.
Text containing another word with some !style_red(additional{} styling).
Text containing yet another word with some !style_red[additional{} styling].

!3 Heading level three
!4 Heading level four
!5 Heading level five
!6 Heading level six

For more info, !see .FrontPage

!c This is a centered line

!note Just a note

above----below
above--------below

!2 Links

!img http://files/fitnesse/images/fitnesse-logo.png

Link to #bottomOfPage using anchor

Click [[Bottom of Page][#bottomOfPage]] to go to bottom

.RootPage.ChildPage
SameLevelPage.ChildPage
>ChildPage
[[Click to view Recent Changes][RecentChanges]]

!2 Collapsibles

!****** [open by default]
 multi-line wiki text
 N.B.: Multiple asterisks are allowed
*!

!*> [collapsed by default]
 multi-line wiki text
*!

!*< [hidden by default]
 multi-line wiki text
*!

!2 Lists

 * Item one
  * sub item one
  * sub item two

 * Item two.
  1 sub item 2
  2 sub item 3
   * sub sub item one
    1 sub sub sub item one.


!2 Formatting

The text !-'''bold'''-!	would not be in bold, since it is literal text.

!-
Literal text
can even span
multiple lines
-!

!- !- Nested Literal Text -! -!

{{{
public class AddRemovePlayerFixture extends ColumnFixture {
  public String playerName;
  private Game theGame;

  public boolean addPlayer() {
    theGame = StaticGame.getInstance();
    Player thePlayer = theGame.addPlayer(playerName);
    return theGame.playerIsPlaying(thePlayer);
  }

  public int countPlayers() {
    return theGame.getNumberOfPlayers();
  }
}
}}}

#
# Just a comment with a $varVariable inlined.
#

!2 Variables

!define markedUp {This is '''bold'''}
!define y {5}
!define z y
!define d {${= ${y} / ${z} =}}

You express the value of a variable like this: ${y}. This expression is replaced with the value of the variable.
For example, ${d} would be replaced with the value 1 (5/5).

!2 Commands

!help
!lastmodified
!contents -g
!include -seamless IncludeAsSnippet

!path java.path.spec
!fixture path.fixture

!|Render table as-is|
|So no $varVariable here|
|!-Or Literal Text-!|
|#or comments|


!anchor bottomOfPage