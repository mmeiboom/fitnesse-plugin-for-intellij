package nl.mmeiboom.fitnesseplugin;

import com.intellij.lexer.Lexer;
import com.intellij.testFramework.LexerTestCase;

import nl.mmeiboom.fitnesseplugin.parser.FitnesseLexerAdapter;

/**
 * Util to manually run a lexer against some file contents
 */
public class FitnesseSingleLexingTest extends LexerTestCase {

    public void testLexingWikiText() {
        String in = "!*> '''Pre condition'''\n" +
                "!today (jemoeder)" +
                "|Random Value                    |\n" +
                "|min     |max     |value?        |\n" +
                "|66680000|66680009|$varGlobalId1=|\n" +
                "|66680010|66680019|$varGlobalId2=|\n" +
                "|66680020|66680029|$varGlobalId3=|\n" +
                "|15000   |16000   |$varBrick=    |\n" +
                "|17000   |18000   |$varBrick2=   |\n" +
                "|19000   |20000   |$varBrick3=   |\n" +
                "\n" +
                "|script            |\n" +
                "|$varLdrId= |echo|2|\n" +
                "|$varLdrId2=|echo|9|\n" +
                "\n" +
                "|script                                                                                                     |\n" +
                "|Data clean up for relation between dropshipper|$varLdrId    |and gpc_brick|$varBrick |from the FIM database|\n" +
                "|Data clean up for relation between dropshipper|$varLdrId2   |and gpc_brick|$varBrick2|from the FIM database|\n" +
                "|Data clean up for relation between dropshipper|$varLdrId    |and gpc_brick|$varBrick3|from the FIM database|\n" +
                "|Data clean up for gpc_brick                   |$varBrick    |from the FIM database                         |\n" +
                "|Data clean up for gpc_brick                   |$varBrick2   |from the FIM database                         |\n" +
                "|Data clean up for gpc_brick                   |$varBrick3   |from the FIM database                         |\n" +
                "|Data clean up for global id                   |$varGlobalId1|from the FIM database                         |\n" +
                "|Data clean up for global id                   |$varGlobalId2|from the FIM database                         |\n" +
                "|Data clean up for global id                   |$varGlobalId3|from the FIM database                         |\n" +
                "|Data clean up for loi id                      |1001         |from the FIM database                         |\n" +
                "|Data clean up for loi id                      |1002         |from the FIM database                         |\n" +
                "|Data clean up for loi id                      |1003         |from the FIM database                         |\n" +
                "\n" +
                "\n" +
                "\n" +
                "|script                                                                                                                                                                                                  |\n" +
                "|Determine current date and store date in        |$today       |week in                            |$week       |month in|$month|year in|$IsoYear                                                        |\n" +
                "|insert global id                                |$varGlobalId1|into the product database with name|FimUC001TC06|brand   |TST   |price  |5|rank|1|gpc brick|$varBrick |type|TST|and abc class|A|as values|\n" +
                "|insert global id                                |$varGlobalId2|into the product database with name|FimUC001TC06|brand   |TST   |price  |5|rank|1|gpc brick|$varBrick2|type|TST|and abc class|A|as values|\n" +
                "|insert global id                                |$varGlobalId3|into the product database with name|FimUC001TC06|brand   |TST   |price  |5|rank|1|gpc brick|$varBrick3|type|TST|and abc class|A|as values|\n" +
                "|Insert gpc_brick with gbk_number                |$varBrick    |name                               |FimUC001TC06                                                                                         |\n" +
                "|Insert gpc_brick with gbk_number                |$varBrick2   |name                               |FimUC001TC06                                                                                         |\n" +
                "|Insert gpc_brick with gbk_number                |$varBrick3   |name                               |FimUC001TC06                                                                                         |\n" +
                "|Insert dropshipper gpc_brick relation for ldr_id|$varLdrId    |and gbk_number                     |$varBrick                                                                                            |\n" +
                "|Insert dropshipper gpc_brick relation for ldr_id|$varLdrId2   |and gbk_number                     |$varBrick2                                                                                           |\n" +
                "|Insert dropshipper gpc_brick relation for ldr_id|$varLdrId    |and gbk_number                     |$varBrick3                                                                                           |\n" +
                "\n" +
                "|script                                               |\n" +
                "|start  |Sql select        |${environment}_FIM        |\n" +
                "|set sql|!-select to_char(sysdate, 'YYYY') from dual-!|\n" +
                "|$year= |execute single result                        |\n" +
                "\n" +
                "*!\n" +
                "\n" +
                "!3 '''Start situation'''\n" +
                "!4 Multiple sales notification xmls are created\n" +
                "\n" +
                "\n" +
                "!3 '''Action'''\n" +
                "!4 '''Process multiple sales notification messages to the Interface & Maintenance Component'''\n" +
                "\n" +
                "|script                                  |\n" +
                "|Set sales notification messages with xml|!-<SalesNotification>\n" +
                "  <lor_id>1001</lor_id>\n" +
                "  <loi_id>1001</loi_id>\n" +
                "  <global_id>$varGlobalId1</global_id>\n" +
                "  <order_date>$today</order_date>\n" +
                "  <billing_country>BE</billing_country>\n" +
                "  <product_type>C</product_type>\n" +
                "  <quan_requested>3</quan_requested>\n" +
                "  <quan_invoiced>0</quan_invoiced>\n" +
                "  <quan_cancelled>0</quan_cancelled>\n" +
                "  <revenue>19</revenue>\n" +
                "  <old_status>XX</old_status>\n" +
                "  <new_status>21</new_status>\n" +
                "  <registered>Y</registered>\n" +
                "  <state>1</state>\n" +
                "</SalesNotification>\n" +
                "-!|and|!-<SalesNotification>\n" +
                "  <lor_id>1002</lor_id>\n" +
                "  <loi_id>1002</loi_id>\n" +
                "  <global_id>$varGlobalId2</global_id>\n" +
                "  <order_date>$today</order_date>\n" +
                "  <billing_country>BE</billing_country>\n" +
                "  <product_type>C</product_type>\n" +
                "  <quan_requested>3</quan_requested>\n" +
                "  <quan_invoiced>0</quan_invoiced>\n" +
                "  <quan_cancelled>0</quan_cancelled>\n" +
                "  <revenue>19</revenue>\n" +
                "  <old_status>XX</old_status>\n" +
                "  <new_status>21</new_status>\n" +
                "  <registered>Y</registered>\n" +
                "  <state>3</state>\n" +
                "</SalesNotification>-!|and|!-<SalesNotification>\n" +
                "  <lor_id>1003</lor_id>\n" +
                "  <loi_id>1003</loi_id>\n" +
                "  <global_id>$varGlobalId3</global_id>\n" +
                "  <order_date>$today</order_date>\n" +
                "  <billing_country>BE</billing_country>\n" +
                "  <product_type>C</product_type>\n" +
                "  <quan_requested>3</quan_requested>\n" +
                "  <quan_invoiced>0</quan_invoiced>\n" +
                "  <quan_cancelled>0</quan_cancelled>\n" +
                "  <revenue>19</revenue>\n" +
                "  <old_status>XX</old_status>\n" +
                "  <new_status>21</new_status>\n" +
                "  <registered>Y</registered>\n" +
                "  <state>2</state>\n" +
                "  <condition>6</condition>\n" +
                "</SalesNotification>\n" +
                "-!|in the processing module in FIM|\n" +
                "\n" +
                "!3 '''Result'''\n" +
                "!4 '''All customer order items are processed and available in the Interface & Maintenance Component'''\n" +
                "\n" +
                "|Query:Sql select|${environment}_FIM|select * from fim.fim_customer_order_items where global_id in ($varGlobalId1, $varGlobalId2, $varGlobalId3)|10                                                        |\n" +
                "|LOI_ID          |LOR_ID            |GLOBAL_ID                                                                                                  |MONTH |QUANTITY|ISO_WEEK|YEAR |CHANNEL|LDR_ID    |ISO_YEAR|\n" +
                "|1001            |1001              |$varGlobalId1                                                                                              |$month|3       |$week   |$year|B      |$varLdrId |$IsoYear|\n" +
                "|1002            |1002              |$varGlobalId2                                                                                              |$month|3       |$week   |$year|B      |$varLdrId2|$IsoYear|\n" +
                "|1003            |1003              |$varGlobalId3                                                                                              |$month|3       |$week   |$year|P      |$varLdrId |$IsoYear|\n" +
                "\n" +
                "!*> '''Post condition'''\n" +
                "\n" +
                "|script                                                                                                     |\n" +
                "|Data clean up for relation between dropshipper|$varLdrId    |and gpc_brick|$varBrick |from the FIM database|\n" +
                "|Data clean up for relation between dropshipper|$varLdrId2   |and gpc_brick|$varBrick2|from the FIM database|\n" +
                "|Data clean up for relation between dropshipper|$varLdrId    |and gpc_brick|$varBrick3|from the FIM database|\n" +
                "|Data clean up for gpc_brick                   |$varBrick    |from the FIM database                         |\n" +
                "|Data clean up for gpc_brick                   |$varBrick2   |from the FIM database                         |\n" +
                "|Data clean up for gpc_brick                   |$varBrick3   |from the FIM database                         |\n" +
                "|Data clean up for global id                   |$varGlobalId1|from the FIM database                         |\n" +
                "|Data clean up for global id                   |$varGlobalId2|from the FIM database                         |\n" +
                "|Data clean up for global id                   |$varGlobalId3|from the FIM database                         |\n" +
                "|Data clean up for loi id                      |1001         |from the FIM database                         |\n" +
                "|Data clean up for loi id                      |1002         |from the FIM database                         |\n" +
                "|Data clean up for loi id                      |1003         |from the FIM database                         |\n" +
                "*!"; // File contents
        String ast = ""; // Expected AST

        doTest(in, ast);
    }

    @Override
    protected Lexer createLexer() {
        return new FitnesseLexerAdapter();
    }

    @Override
    protected String getDirPath() {
        return "";
    }
}
