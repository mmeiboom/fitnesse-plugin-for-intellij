package nl.mmeiboom.fitnesseplugin;

import com.intellij.lexer.Lexer;
import com.intellij.testFramework.LexerTestCase;
import nl.mmeiboom.fitnesseplugin.parser.FitnesseLexerAdapter;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Runs lexing test against all registered files.
 * Testdata is shared with the parser testsuite.
 */
@RunWith(Parameterized.class)
public class FitnesseBulkLexingTest extends LexerTestCase {

    private String in;
    private String ast;

    public FitnesseBulkLexingTest(String testSuite, String testCase) throws IOException {
        this.in = retrieveFileContent("/testdata/" + testSuite + "/" + testCase + ".wiki");
        this.ast = retrieveFileContent("/testdata/" + testSuite + "/ast/" + testCase + ".ast");
    }

    @Test
    public void runTest() throws Exception {
        doTest(in, ast);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        List<Object[]> parameters = new ArrayList<Object[]>();

        parameters.add(new Object[]{"table_parsing_test_case", "CollapsedTable"});
        parameters.add(new Object[]{"table_parsing_test_case", "MultilineComments"});

        return parameters;
    }

    @Override
    protected Lexer createLexer() {
        return new FitnesseLexerAdapter();
    }

    @Override
    protected String getDirPath() {
        return "";
    }

    private static String retrieveFileContent(String fileName) throws IOException {
        return IOUtils.toString(FitnesseBulkLexingTest.class.getResourceAsStream(fileName));
    }
}
