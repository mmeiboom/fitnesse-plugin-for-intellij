package nl.mmeiboom.fitnesseplugin.parser;

/**
 * Base class for creating parser tests.
 */
public abstract class ParsingTestCase extends com.intellij.testFramework.ParsingTestCase {
    public ParsingTestCase() {
        super("", "wiki", new FitnesseParserDefinition());
    }

    @Override
    protected String getTestDataPath() {
        return "tests/resources/testdata";
    }

    @Override
    protected boolean skipSpaces() {
        return false;
    }

    @Override
    protected boolean includeRanges() {
        return true;
    }
}