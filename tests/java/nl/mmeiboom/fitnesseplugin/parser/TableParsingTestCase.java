package nl.mmeiboom.fitnesseplugin.parser;

public class TableParsingTestCase extends ParsingTestCase {

    public void testVariableParsing() { doTest(true); }

    public void testScriptTable() {
        doTest(true);
    }

    public void testScenarioTable() {
        doTest(true);
    }

    public void testHiddenHeaderScriptTable() {
        doTest(true);
    }

    public void testFullSyntaxSample() { doTest(true); }

    public void testCollapsedTable() {
        doTest(true);
    }

    public void testPushback() { doTest(true); }

    @Override
    protected String getTestDataPath() {
        return super.getTestDataPath() + "/table_parsing_test_case";
    }
}
