package nl.mmeiboom.fitnesseplugin.editor;

import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.fileEditor.impl.EditorTabTitleProvider;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.VirtualFile;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nullable;

import java.io.File;

/**
 * Provides a title for editor tabs.
 *
 * This will replace the tab title for common FitNesse files such
 * as editor.txt with an indication of the folder it is placed in.
 * This way, it should be easier to distinguish between testcases.
 */
public class FitnesseTabTitleProvider implements EditorTabTitleProvider {

    private final Logger log = Logger.getInstance(this.getClass().getCanonicalName());

    /**
     * Returns a replacement tab title, or null in case we can not determine
     * a sensible title (effectively giving responsibility back to the IDE).
     *
     * @param project
     * @param file
     * @return Either a String containing the tab title, or null
     */
    @Nullable
    @Override
    public String getEditorTabTitle(Project project, VirtualFile file) {
        if(isTestCase(file)) {
            return getNameOfContainingDirectory(file);
        }
        return null;
    }

  private boolean isTestCase(VirtualFile file) {

    boolean fileIsLocatedInWikiDirectory = file.getPath().contains("/FrontPage/");
    boolean fileContainsWikiPage = file.getPresentableName().equals("content.txt");

    return fileIsLocatedInWikiDirectory && fileContainsWikiPage;
  }

  /**
     * Returns the name of the directory containing a file
     *
     * @param file
     * @return String containing the name of the containing directory
     */
    private String getNameOfContainingDirectory(VirtualFile file) {
        String filePath = FileUtil.toSystemDependentName(file.getPath());
        String[] parts = StringUtils.split(filePath, File.separatorChar);

        StringBuilder sb = new StringBuilder();
        if (parts.length > 1) {
            sb.append("[");
            sb.append(parts[parts.length - 2]);
            sb.append("] ");
        }
        sb.append(file.getPresentableName());
        return sb.toString();
    }
}
