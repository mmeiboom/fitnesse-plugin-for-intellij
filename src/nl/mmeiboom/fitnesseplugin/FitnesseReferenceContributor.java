package nl.mmeiboom.fitnesseplugin;

import com.intellij.openapi.util.TextRange;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.*;
import com.intellij.util.ProcessingContext;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseSymbol;
import nl.mmeiboom.fitnesseplugin.psi.impl.FitnesseSymbolImpl;
import org.jetbrains.annotations.NotNull;

public class FitnesseReferenceContributor extends PsiReferenceContributor {
  @Override
  public void registerReferenceProviders(PsiReferenceRegistrar registrar) {

    registrar.registerReferenceProvider(PlatformPatterns.psiElement(FitnesseSymbolImpl.class),
        new PsiReferenceProvider() {
          @NotNull
          @Override
          public PsiReference[] getReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
            return new PsiReference[]{new FitnesseSymbolReference(element)};
          }
        });

  }
}