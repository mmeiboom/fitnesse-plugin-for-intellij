package nl.mmeiboom.fitnesseplugin.parser;

import java.util.Stack;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.TokenType;

import nl.mmeiboom.fitnesseplugin.psi.FitnesseTypes;

%%

%class FitnesseLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}

%{
    private Stack<Integer> stack = new Stack<Integer>();

    public void yypushState(int newState) {
      stack.push(yystate());
      yybegin(newState);
    }

    public void yypopState() {
      yybegin(stack.pop());
    }
%}

CRLF= \n|\r|\r\n
WS=[\ \t\f]
FIRST_VALUE_CHARACTER=[^ \n\r\f\\] | "\\"{CRLF} | "\\".
VALUE_CHARACTER=[^\n\r\f\\] | "\\"{CRLF} | "\\".
LINE_COMMENT=("#")[^\r\n]*
SEPARATOR=[:=]
LSS="!-"
LSE="-!"

%state WAITING_VALUE
%state TABLE
%state INCLUDE
%state ANCHOR
%state COMMAND
%state CONTENTS
%state TABLE_FIRST_ROW
%state TABLE_ROW
%state TABLE_CELL
%state WAITING_FOR_SYMBOL
%state WAITING_FOR_VARIABLE
%state WAITING_FOR_DEFINE_VAR
%state WAITING_FOR_DEFINE_DEF
%state LITERAL_TEXT

%%

<YYINITIAL> {

    // Commands
    ^ "!"/[^*|-]                                        { yybegin(COMMAND); yypushback(1); }

    ^ ("!"|"-")? "|"                                    { yybegin(TABLE_FIRST_ROW); return FitnesseTypes.TABLE_START; }

    ^"!*" ">"?                                          { return FitnesseTypes.COLLAPSABLE_START; }

    ^"*!"                                               { return FitnesseTypes.COLLAPSABLE_END; }

    {LINE_COMMENT}                                      { yybegin(YYINITIAL); return FitnesseTypes.COMMENT; }

    {SEPARATOR}                                         { yybegin(WAITING_VALUE); return FitnesseTypes.SEPARATOR; }

    {CRLF}                                              { yybegin(YYINITIAL); return FitnesseTypes.CRLF; }

    {WS}+                                               { /* ignore */ }

    {LSS}                                               { yypushState(LITERAL_TEXT); return FitnesseTypes.LITERAL_TEXT; }

    "{{{" ~"}}}"                                        { return FitnesseTypes.TEXT; }

    ^ [^-!|\n\r] [^\n\r]    +                           { return FitnesseTypes.TEXT; }

    [^!\n\r|$ \f\t]+                                    { return FitnesseTypes.TEXT; }

    "${"                                                { yypushState(WAITING_FOR_VARIABLE); return FitnesseTypes.VARIABLE_SIGN; }

    "$"                                                 { yypushState(WAITING_FOR_SYMBOL); return FitnesseTypes.SYMBOL_SIGN; }
}

<INCLUDE> {
    {WS}+ "-c"                                          { return FitnesseTypes.PARAM; }

    {WS}+ "-seamless"                                   { return FitnesseTypes.PARAM; }

    {WS}+ (("." | ">")? [:jletterdigit:]+)+             { return FitnesseTypes.INCLUDE_PATH; }

    {CRLF}                                              { yybegin(YYINITIAL); return FitnesseTypes.CRLF; }

    .                                                   { yybegin(YYINITIAL); yypushback(yylength()); }
}

<COMMAND> {

    "!include"/{WS}                                      { yybegin(INCLUDE); return FitnesseTypes.INCLUDE_START; }

    "!contents"/{WS}                                     { yybegin(CONTENTS); return FitnesseTypes.CONTENTS_START; }

    "!define" {WS}+                                      { yybegin(WAITING_FOR_DEFINE_VAR); return FitnesseTypes.DEFINE_START; }

    "!anchor" {WS}+                                      { yybegin(ANCHOR); return FitnesseTypes.ANCHOR_START; }

    "!help" {WS}* ({WS}"-editable")?                     { yybegin(YYINITIAL); return FitnesseTypes.GENERIC_COMMAND; }

    "!lastmodified" | "!today"                           { yybegin(YYINITIAL); return FitnesseTypes.GENERIC_COMMAND; }

    // Headings
    "!" (1|2|3|4|5|6) {WS}*                              { yybegin(YYINITIAL); return FitnesseTypes.HEADING_START; }

    "!"/.                                                { yybegin(YYINITIAL); return FitnesseTypes.TEXT; } // it's just an exclamation mark!
}

<CONTENTS> {
    {WS}+ "-" [:jletterdigit:]+                         { return FitnesseTypes.PARAM; }

    {CRLF}                                              { yybegin(YYINITIAL); return FitnesseTypes.CRLF; }

    .                                                   { yybegin(YYINITIAL); yypushback(1); }
}

<TABLE> {
    "|"                                                 { yybegin(TABLE_CELL); return FitnesseTypes.TABLE_CELL_DELIM;}

    {CRLF}                                              { yybegin(YYINITIAL); return FitnesseTypes.CRLF; }

    .                                                   { yybegin(YYINITIAL); yypushback(1); }
}

<TABLE_ROW> {
    "|"                                                 { return FitnesseTypes.TABLE_CELL_DELIM; }

    {CRLF}                                              { yybegin(TABLE); return FitnesseTypes.CRLF; }

    {WS}+                                               { return TokenType.WHITE_SPACE; }

    .                                                   { yybegin(TABLE_CELL); yypushback(1); }
}

<TABLE_FIRST_ROW> {

    {WS}* "script" {WS}* "|"                            { yybegin(TABLE_ROW); yypushback(1); return FitnesseTypes.TABLE_SCRIPT_START; }

    {WS}* "scenario" {WS}* "|"                          { yybegin(TABLE_ROW); yypushback(1); return FitnesseTypes.TABLE_SCENARIO_START; }

    {WS}* "comment" {WS}* "|"                           { yybegin(TABLE_ROW); yypushback(1); return FitnesseTypes.TABLE_COMMENT_START; }

    {WS}* "import" {WS}* "|"                            { yybegin(TABLE_ROW); yypushback(1); return FitnesseTypes.TABLE_IMPORT_START; }

    {WS}* "library" {WS}* "|"                           { yybegin(TABLE_ROW); yypushback(1); return FitnesseTypes.TABLE_LIBRARY_START; }

    {WS}* ("Query:" | "Ordered Query:" | "Subset Query:") [^\n\r|$]* {WS}* "|"
                                                        { yybegin(TABLE_ROW); yypushback(1); return FitnesseTypes.TABLE_QUERY_START; }
    [^\n\r|$]* "|"                                      { yybegin(TABLE_ROW); yypushback(1); return FitnesseTypes.TABLE_DECISION_START; }

    .                                                   { yybegin(TABLE_ROW); }
}

<TABLE_CELL> {
    {LSS}                                               { yypushState(LITERAL_TEXT); return FitnesseTypes.LITERAL_TEXT; }
    "${"                                                { yypushState(WAITING_FOR_VARIABLE); return FitnesseTypes.VARIABLE_SIGN; }
    "$"                                                 { yypushState(WAITING_FOR_SYMBOL); return FitnesseTypes.SYMBOL_SIGN; }
    "|"                                                 { yybegin(TABLE_ROW); return FitnesseTypes.TABLE_CELL_DELIM; }
    [ \f\t]+                                            { return TokenType.WHITE_SPACE; }
    {CRLF}                                              { yybegin(TABLE); return FitnesseTypes.CRLF; }
    [^\n\r\t\f |$!]+                                    { return FitnesseTypes.TEXT; }
    "!"                                                 { return FitnesseTypes.TEXT; } // LSS is longer, so has precedence
}


<LITERAL_TEXT> {
    "!-"        { yypushState(LITERAL_TEXT); return FitnesseTypes.LITERAL_TEXT; }
    [^\-\!\!]*  { return FitnesseTypes.LITERAL_TEXT; }
    "-!"        { yypopState(); return FitnesseTypes.LITERAL_TEXT; }
    [\-\!\!]    { return FitnesseTypes.LITERAL_TEXT; }
}

<ANCHOR> {
    [:jletter:] [:jletterdigit:]*                       { yybegin(YYINITIAL); return FitnesseTypes.ANCHOR_NAME; }
    .                                                   { yypopState(); return TokenType.BAD_CHARACTER; }
}

<WAITING_FOR_SYMBOL> {
    [:jletter:] [:jletterdigit:]*                       { yypopState(); return FitnesseTypes.SYMBOL_NAME; }
    .                                                   { yypopState(); return TokenType.BAD_CHARACTER; }
}

<WAITING_FOR_DEFINE_VAR> {
    [:jletter:] [:jletterdigit:]*                       { yybegin(WAITING_FOR_DEFINE_DEF); return FitnesseTypes.VARIABLE_NAME; }
    .                                                   { yypopState(); return TokenType.BAD_CHARACTER; }
}

<WAITING_FOR_DEFINE_DEF> {
    [^\n\r]+                                            { return FitnesseTypes.VARIABLE_DEFINITION; }
    {CRLF}                                              { yybegin(YYINITIAL); return FitnesseTypes.CRLF; }
}

<WAITING_FOR_VARIABLE> {
    [:jletter:] [:jletterdigit:]*                       { return FitnesseTypes.VARIABLE_NAME; }

    "}"                                                 { yypopState(); return FitnesseTypes.VARIABLE_END;}

    .                                                   { yypopState(); return TokenType.BAD_CHARACTER; }
}

<WAITING_VALUE> {
    {CRLF}                                              { yybegin(YYINITIAL); return FitnesseTypes.CRLF; }

    {WS}+                                               { /* ignore */ }

    {FIRST_VALUE_CHARACTER}{VALUE_CHARACTER}*           { yybegin(YYINITIAL); return FitnesseTypes.VALUE; }
}

{CRLF}                                                  { return FitnesseTypes.CRLF; }

{WS}+                                                   { /* ignore */ }

//<<EOF>>                                                 { return FitnesseTypes.CRLF; }

.                                                       { yybegin(YYINITIAL); return TokenType.BAD_CHARACTER; }