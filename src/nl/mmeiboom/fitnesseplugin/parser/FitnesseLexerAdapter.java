package nl.mmeiboom.fitnesseplugin.parser;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

public class FitnesseLexerAdapter extends FlexAdapter {
    public FitnesseLexerAdapter() {
        super(new FitnesseLexer((Reader) null));
    }
}
