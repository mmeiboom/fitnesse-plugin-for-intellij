package nl.mmeiboom.fitnesseplugin.parser;

import com.intellij.lang.ASTNode;
import com.intellij.lang.Language;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import nl.mmeiboom.fitnesseplugin.FitnesseTokenType;
import nl.mmeiboom.fitnesseplugin.lang.FitnesseLanguage;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTypes;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseFile;
import org.jetbrains.annotations.NotNull;

public class FitnesseParserDefinition implements ParserDefinition{
    public static final TokenSet WHITE_SPACES = TokenSet.create(TokenType.WHITE_SPACE);
    public static final TokenSet COMMENTS = TokenSet.create(FitnesseTypes.COMMENT);

    public static final IFileElementType FILE = new IFileElementType(Language.<FitnesseLanguage>findInstance(FitnesseLanguage.class));

    @NotNull
    @Override
    public Lexer createLexer(Project project) {
        return new FitnesseLexerAdapter();
    }

    @NotNull
    public TokenSet getWhitespaceTokens() {
        return WHITE_SPACES;
    }

    @NotNull
    public TokenSet getCommentTokens() {
        return COMMENTS;
    }

    @NotNull
    public TokenSet getStringLiteralElements() {
        return TokenSet.EMPTY;
    }

    @NotNull
    public PsiParser createParser(final Project project) {
        return new FitnesseParser();
    }

    @Override
    public IFileElementType getFileNodeType() {
        return FILE;
    }

    public PsiFile createFile(FileViewProvider viewProvider) {
        return new FitnesseFile(viewProvider);
    }

    public SpaceRequirements spaceExistanceTypeBetweenTokens(ASTNode left, ASTNode right) {
        return SpaceRequirements.MAY;
    }

    @NotNull
    public PsiElement createElement(ASTNode node) {
        return FitnesseTypes.Factory.createElement(node);
    }
}