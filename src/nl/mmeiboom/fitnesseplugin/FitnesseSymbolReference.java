package nl.mmeiboom.fitnesseplugin;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementResolveResult;
import com.intellij.psi.PsiPolyVariantReference;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.psi.ResolveResult;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseSymbol;
import nl.mmeiboom.fitnesseplugin.psi.impl.FitnesseSymbolImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FitnesseSymbolReference extends PsiReferenceBase<PsiElement> implements PsiPolyVariantReference {

  public FitnesseSymbolReference(@NotNull PsiElement element) {
    super(element);
  }

  @NotNull
  @Override
  public ResolveResult[] multiResolve(boolean incompleteCode) {
    Project project = myElement.getProject();
    final Map<String, FitnesseSymbolImpl> symbols = FitnesseUtil.findSymbols(project);
    List<ResolveResult> results = new ArrayList<ResolveResult>();
    for (FitnesseSymbol symbol : symbols.values()) {
      results.add(new PsiElementResolveResult(symbol));
    }
    return results.toArray(new ResolveResult[results.size()]);
  }

  @Nullable
  @Override
  public PsiElement resolve() {
    ResolveResult[] resolveResults = multiResolve(false);
    return resolveResults.length == 1 ? resolveResults[0].getElement() : null;
  }

  @NotNull
  @Override
  public Object[] getVariants() {
    Project project = myElement.getProject();
    Map<String, FitnesseSymbolImpl> symbols = FitnesseUtil.findSymbols(project);
    List<LookupElement> variants = new ArrayList<LookupElement>();
    for (final FitnesseSymbolImpl symbol : symbols.values()) {
      if (symbol.getSymbolName() != null && symbol.getSymbolName().length() > 0) {
        variants.add(LookupElementBuilder.create(symbol).
                withIcon(FitnesseIcons.FILE).
                withTypeText(symbol.getContainingFile().getName())
        );
      }
    }
    return variants.toArray();
  }
}