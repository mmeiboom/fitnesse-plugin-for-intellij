package nl.mmeiboom.fitnesseplugin;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class FitnesseIcons {
    public static final Icon FILE = IconLoader.getIcon("/nl/mmeiboom/fitnesseplugin/icons/fit.png");
}