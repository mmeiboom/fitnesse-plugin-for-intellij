package nl.mmeiboom.fitnesseplugin;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.ArrayUtil;
import com.intellij.util.SmartList;
import com.intellij.util.indexing.FileBasedIndex;
import nl.mmeiboom.fitnesseplugin.lang.FitnesseFileType;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseProperty;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseFile;
import nl.mmeiboom.fitnesseplugin.psi.impl.FitnesseSymbolImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FitnesseUtil {
    public static List<FitnesseProperty> findProperties(Project project, String key) {
        List<FitnesseProperty> result = null;
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, FitnesseFileType.INSTANCE,
                GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            FitnesseFile fitnesseFile = (FitnesseFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (fitnesseFile != null) {
                FitnesseProperty[] properties = PsiTreeUtil.getChildrenOfType(fitnesseFile, FitnesseProperty.class);
                if (properties != null) {
                    for (FitnesseProperty property : properties) {
                        if (key.equals(property.getKey())) {
                            if (result == null) {
                                result = new ArrayList<FitnesseProperty>();
                            }
                            result.add(property);
                        }
                    }
                }
            }
        }
        return result != null ? result : Collections.<FitnesseProperty>emptyList();
    }

    public static List<FitnesseProperty> findProperties(Project project) {
        List<FitnesseProperty> result = new ArrayList<FitnesseProperty>();
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, FitnesseFileType.INSTANCE,
                GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            FitnesseFile fitnesseFile = (FitnesseFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (fitnesseFile != null) {
                FitnesseProperty[] properties = PsiTreeUtil.getChildrenOfType(fitnesseFile, FitnesseProperty.class);
                if (properties != null) {
                    Collections.addAll(result, properties);
                }
            }
        }
        return result;
    }

    public static Map<String, FitnesseSymbolImpl> findSymbols(Project project) {
        Map<String, FitnesseSymbolImpl> result = new HashMap<String, FitnesseSymbolImpl>();
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, FitnesseFileType.INSTANCE,
                GlobalSearchScope.allScope(project));

        for (VirtualFile virtualFile : virtualFiles) {
            FitnesseFile fitnesseFile = (FitnesseFile) PsiManager.getInstance(project).findFile(virtualFile);

            Collection<FitnesseSymbolImpl> symbols = PsiTreeUtil.findChildrenOfAnyType(fitnesseFile, FitnesseSymbolImpl.class);
            for (FitnesseSymbolImpl symbol : symbols) {
                result.put(symbol.getName(), symbol);
            }

        }

        return result;
    }

    public static <T extends PsiElement> T[] getChildrenOfType(@Nullable PsiElement element, @NotNull Class<T> aClass) {
        if (element == null) return null;

        List<T> result = null;
        for (PsiElement child = element.getFirstChild(); child != null; child = child.getNextSibling()) {
            if (aClass.isInstance(child)) {
                if (result == null) result = new SmartList<T>();
                //noinspection unchecked
                result.add((T) child);
            }
        }
        return result == null ? null : ArrayUtil.toObjectArray(result, aClass);
    }
}