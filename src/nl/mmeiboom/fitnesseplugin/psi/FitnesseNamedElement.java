package nl.mmeiboom.fitnesseplugin.psi;

import com.intellij.psi.PsiNameIdentifierOwner;

public interface FitnesseNamedElement extends PsiNameIdentifierOwner {
}