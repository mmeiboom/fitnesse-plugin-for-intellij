package nl.mmeiboom.fitnesseplugin.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.impl.source.resolve.reference.ReferenceProvidersRegistry;
import com.intellij.psi.tree.TokenSet;
import nl.mmeiboom.fitnesseplugin.formatter.FormattingException;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseImportTable;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTable;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTableBody;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTableCell;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTableElement;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTableHeaderElement;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTableRow;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTypes;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public abstract class FitnesseTableHeaderElementImpl extends ASTWrapperPsiElement implements FitnesseTableHeaderElement {
  public FitnesseTableHeaderElementImpl(@NotNull ASTNode node) {
    super(node);
  }

  @Nullable
  public PsiReference getReference() {
    PsiReference[] references = getReferences();
    return references.length == 0 ? null : references[0];
  }

  @NotNull
  public PsiReference[] getReferences() {
    return ReferenceProvidersRegistry.getReferencesFromProviders(this);
  }

  public int getColumnCount() throws FormattingException {
    List<PsiElement> delimiters = this.findChildrenByType(FitnesseTypes.TABLE_CELL_DELIM);
    return delimiters.size();
  }

  public int[] getColumnWidths() throws FormattingException {
    int[] columnWidths = new int[getColumnCount()];
    int i = 0;

    columnWidths[i] = this.getFirstChild().getText().trim().length();
    List<PsiElement> cells = this.findChildrenByType(FitnesseTypes.TABLE_CELL);
    for(PsiElement cell : cells) {
      i++;
      columnWidths[i] = cell.getText().trim().length();
    }
    return columnWidths;
  }
}