package nl.mmeiboom.fitnesseplugin.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiReference;
import com.intellij.psi.impl.source.resolve.reference.ReferenceProvidersRegistry;
import nl.mmeiboom.fitnesseplugin.formatter.FormattingException;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseNamedElement;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTable;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTableBody;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTableCell;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTableElement;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTableRow;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.Marshaller;
import java.util.List;

public abstract class FitnesseTableElementImpl extends ASTWrapperPsiElement implements FitnesseTableElement {
  public FitnesseTableElementImpl(@NotNull ASTNode node) {
    super(node);
  }

  @Nullable
  public PsiReference getReference() {
    PsiReference[] references = getReferences();
    return references.length == 0 ? null : references[0];
  }

  @NotNull
  public PsiReference[] getReferences() {
    return ReferenceProvidersRegistry.getReferencesFromProviders(this);
  }

  public int getColumnCount() throws FormattingException {
    List<FitnesseTableRow> tableRowList = getTableRowList();
    int maxColumnCount = getHeader().getColumnCount();
    for (FitnesseTableRow tableRow : tableRowList) {
      maxColumnCount = Math.max(maxColumnCount, tableRow.getTableCellList().size());
    }
    return maxColumnCount;
  }

  public List<FitnesseTableRow> getTableRowList() throws FormattingException {
    FitnesseTableBody body = getTableBody();
    if(body == null) {
      throw new FormattingException();
    }
    return body.getTableRowList();
  }

  private FitnesseTableBody getTableBody() throws FormattingException {
    // Sanity check, should be enforced by grammar
    if(!(this instanceof FitnesseTable)) {
      throw new FormattingException();
    }

    // TODO this feels exhaustive...
    FitnesseTable table = (FitnesseTable) this;
    if(table.getScriptTable() != null) {
      return table.getScriptTable().getTableBody();
    }
    if(table.getDecisionTable() != null) {
      return table.getDecisionTable().getTableBody();
    }
    if(table.getQueryTable() != null) {
      return table.getQueryTable().getTableBody();
    }
    if(table.getImportTable() != null) {
      return table.getImportTable().getTableBody();
    }
    if(table.getLibraryTable() != null) {
      return table.getLibraryTable().getTableBody();
    }
    if(table.getCommentTable() != null) {
      return table.getCommentTable().getTableBody();
    }
    if(table.getScenarioTable() != null) {
      return table.getScenarioTable().getTableBody();
    }
    throw new FormattingException();
  }

  public int[] getColumnWidths() throws FormattingException {
    List<FitnesseTableRow> tableRowList = getTableRowList();
    int[] cellWidths = new int[getColumnCount()];
    int i = 0;

    // First load header widths into result
    for(int w : getHeader().getColumnWidths()) {
      cellWidths[i] = w;
      i++;
    }

    // Then iterate rows, to find largest cells
    for (FitnesseTableRow tableRow : tableRowList) {
        i = 0;
        for(FitnesseTableCell cell : tableRow.getTableCellList()) {
            cellWidths[i] = Math.max(cellWidths[i], cell.getText().trim().length());
            i++;
        }
    }
    return cellWidths;
  }

  public FitnesseTableHeaderElementImpl getHeader() {
    FitnesseTable table = (FitnesseTable) this;
    if(table.getScriptTable() != null) {
      return (FitnesseTableHeaderElementImpl) table.getScriptTable().getScriptHead();
    }
    if(table.getDecisionTable() != null) {
      return (FitnesseTableHeaderElementImpl) table.getDecisionTable().getDecisionHead();
    }
    if(table.getQueryTable() != null) {
      return (FitnesseTableHeaderElementImpl) table.getQueryTable().getQueryHead();
    }
    if(table.getImportTable() != null) {
      return (FitnesseTableHeaderElementImpl) table.getImportTable().getImportHead();
    }
    if(table.getLibraryTable() != null) {
      return (FitnesseTableHeaderElementImpl) table.getLibraryTable().getLibraryHead();
    }
    if(table.getCommentTable() != null) {
      return (FitnesseTableHeaderElementImpl) table.getCommentTable().getCommentHead();
    }
    if(table.getScenarioTable() != null) {
      return (FitnesseTableHeaderElementImpl) table.getScenarioTable().getScenarioHead();
    }
    return null;
  }
}