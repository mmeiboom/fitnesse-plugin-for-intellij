package nl.mmeiboom.fitnesseplugin.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.PsiElement;
import nl.mmeiboom.fitnesseplugin.FitnesseIcons;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseElementFactory;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseNamedElement;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseProperty;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseSymbol;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTypes;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseVariable;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class FitnessePsiImplUtil {
  public static String getKey(FitnesseProperty element) {
    ASTNode keyNode = element.getNode().findChildByType(FitnesseTypes.KEY);
    if (keyNode != null) {
      return keyNode.getText();
    } else {
      return null;
    }
  }

  public static String getValue(FitnesseProperty element) {
    ASTNode valueNode = element.getNode().findChildByType(FitnesseTypes.VALUE);
    if (valueNode != null) {
      return valueNode.getText();
    } else {
      return null;
    }
  }

  public static String getSymbolName(FitnesseSymbol element) {
    ASTNode nameNode = element.getNode().findChildByType(FitnesseTypes.SYMBOL_NAME);
    if (nameNode != null) {
      return nameNode.getText();
    } else {
      return null;
    }
  }

  public static PsiElement setSymbolName(FitnesseSymbol element, String newName) {
    ASTNode nameNode = element.getNode().findChildByType(FitnesseTypes.SYMBOL_NAME);
    if (nameNode != null) {
      FitnesseSymbol symbol = FitnesseElementFactory.createSymbol(element.getProject(), newName);
      ASTNode newSymbolNode = symbol.getFirstChild().getNextSibling().getNode();
      element.getNode().replaceChild(nameNode, newSymbolNode);
    }
    return element;
  }

  public static String getVariableName(FitnesseVariable element) {
    ASTNode nameNode = element.getNode().findChildByType(FitnesseTypes.VARIABLE_NAME);
    if (nameNode != null) {
      return nameNode.getText();
    } else {
      return null;
    }
  }

  public static PsiElement setVariableName(FitnesseVariable element, String newName) {
    ASTNode nameNode = element.getNode().findChildByType(FitnesseTypes.VARIABLE_NAME);
    if (nameNode != null) {
      FitnesseVariable var = FitnesseElementFactory.createVariable(element.getProject(), newName);
      ASTNode newVariableNode = var.getFirstChild().getNextSibling().getNode();
      element.getNode().replaceChild(nameNode, newVariableNode);
    }
    return element;
  }

  public static String getName(FitnesseNamedElement element) {
    if (element instanceof FitnesseProperty) {
      return getKey((FitnesseProperty) element);
    } else if (element instanceof FitnesseSymbol) {
      return getSymbolName((FitnesseSymbol)element);
    } else if (element instanceof FitnesseVariable) {
      return getVariableName((FitnesseVariable) element);
    }
    return "";
  }

  public static PsiElement setName(FitnesseNamedElement element, String newName) {
    if (element instanceof FitnesseProperty) {
      return setKey((FitnesseProperty)element, newName);
    } else if (element instanceof FitnesseSymbol) {
      return setSymbolName((FitnesseSymbol)element, newName);
    } else if (element instanceof FitnesseVariable) {
      return setVariableName((FitnesseVariable) element, newName);
    }
    return null;
  }

  public static PsiElement setKey(FitnesseProperty element, String newName) {
    ASTNode keyNode = element.getNode().findChildByType(FitnesseTypes.KEY);
    if (keyNode != null) {

      FitnesseProperty property = FitnesseElementFactory.createProperty(element.getProject(), newName);
      ASTNode newKeyNode = property.getFirstChild().getNode();
      element.getNode().replaceChild(keyNode, newKeyNode);
    }
    return element;
  }

  public static PsiElement getNameIdentifier(FitnesseNamedElement element) {

    ASTNode keyNode = null;
    if(element instanceof FitnesseProperty) {
      keyNode = element.getNode().findChildByType(FitnesseTypes.KEY);
    } else if (element instanceof FitnesseSymbol) {
      keyNode = element.getNode().findChildByType(FitnesseTypes.SYMBOL_NAME);
    } else if (element instanceof FitnesseVariable) {
      keyNode = element.getNode().findChildByType(FitnesseTypes.VARIABLE_NAME);
    }
    if (keyNode != null) {
      return keyNode.getPsi();
    } else {
      return null;
    }
  }

  public static ItemPresentation getPresentation(final FitnesseNamedElement element) {
    return new ItemPresentation() {
      @Nullable
      @Override
      public String getPresentableText() {
        return element.getName();
      }

      @Nullable
      @Override
      public String getLocationString() {
        return element.getContainingFile().getName();
      }

      @Nullable
      @Override
      public Icon getIcon(boolean unused) {
        return FitnesseIcons.FILE;
      }
    };
  }
}