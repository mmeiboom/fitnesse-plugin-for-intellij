package nl.mmeiboom.fitnesseplugin.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiReference;
import com.intellij.psi.impl.source.resolve.reference.ReferenceProvidersRegistry;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseNamedElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class FitnesseNamedElementImpl extends ASTWrapperPsiElement implements FitnesseNamedElement {
  public FitnesseNamedElementImpl(@NotNull ASTNode node) {
    super(node);
  }

  @Nullable
  public PsiReference getReference() {
    PsiReference[] references = getReferences();
    return references.length == 0 ? null : references[0];
  }

  @NotNull
  public PsiReference[] getReferences() {
    return ReferenceProvidersRegistry.getReferencesFromProviders(this);
  }
}