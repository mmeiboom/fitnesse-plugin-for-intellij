package nl.mmeiboom.fitnesseplugin.psi;

import com.intellij.psi.tree.IElementType;
import nl.mmeiboom.fitnesseplugin.lang.FitnesseLanguage;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class FitnesseTokenType extends IElementType {
    public FitnesseTokenType(@NotNull @NonNls String debugName) {
        super(debugName, FitnesseLanguage.INSTANCE);
    }

    @Override
    public String toString() {
        return "FitnesseTokenType." + super.toString();
    }
}