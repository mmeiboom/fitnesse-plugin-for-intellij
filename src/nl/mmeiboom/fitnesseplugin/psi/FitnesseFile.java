package nl.mmeiboom.fitnesseplugin.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import nl.mmeiboom.fitnesseplugin.lang.FitnesseFileType;
import nl.mmeiboom.fitnesseplugin.lang.FitnesseLanguage;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class FitnesseFile extends PsiFileBase {
    public FitnesseFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, FitnesseLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return FitnesseFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "Simple File";
    }

    @Override
    public Icon getIcon(int flags) {
        return super.getIcon(flags);
    }
}