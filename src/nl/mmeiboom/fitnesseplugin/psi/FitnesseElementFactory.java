package nl.mmeiboom.fitnesseplugin.psi;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFileFactory;
import nl.mmeiboom.fitnesseplugin.lang.FitnesseFileType;

public class FitnesseElementFactory {
  public static FitnesseProperty createProperty(Project project, String name, String value) {
    final FitnesseFile file = createFile(project, name + " = " + value);
    return (FitnesseProperty) file.getFirstChild();
  }

  public static FitnesseProperty createProperty(Project project, String name) {
    final FitnesseFile file = createFile(project, name);
    return (FitnesseProperty) file.getFirstChild();
  }

  public static FitnesseSymbol createSymbol(Project project, String name) {
    final FitnesseFile file = createFile(project, "$" + name);
    return (FitnesseSymbol) file.getFirstChild();
  }

  public static FitnesseVariable createVariable(Project project, String name) {
    final FitnesseFile file = createFile(project, "${" + name + "}");
    return (FitnesseVariable) file.getFirstChild();
  }

  public static PsiElement createCRLF(Project project) {
    final FitnesseFile file = createFile(project, "\n");
    return file.getFirstChild();
  }

  public static FitnesseFile createFile(Project project, String text) {
    String name = "dummy.simple";
    return (FitnesseFile) PsiFileFactory.getInstance(project).
        createFileFromText(name, FitnesseFileType.INSTANCE, text);
  }
}