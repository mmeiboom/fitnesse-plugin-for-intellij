package nl.mmeiboom.fitnesseplugin;

import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileTypes.StdFileTypes;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiExpression;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiNewExpression;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.xml.XmlToken;
import com.intellij.util.ProcessingContext;
import nl.mmeiboom.fitnesseplugin.lang.FitnesseLanguage;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTypes;
import org.jetbrains.annotations.NotNull;

public class FitnesseCompletionContributor extends CompletionContributor {

    public FitnesseCompletionContributor() {

        extend(CompletionType.BASIC,
                PlatformPatterns.psiElement(FitnesseTypes.TABLE).withLanguage(FitnesseLanguage.INSTANCE),
                new CompletionProvider<CompletionParameters>() {
                    public void addCompletions(@NotNull CompletionParameters parameters,
                                               ProcessingContext context,
                                               @NotNull CompletionResultSet resultSet) {


                        resultSet.addElement(LookupElementBuilder.create("SYMBOL"));
                    }
                }
        );
    }
}
