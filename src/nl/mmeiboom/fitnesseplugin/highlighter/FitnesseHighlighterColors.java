package nl.mmeiboom.fitnesseplugin.highlighter;

import com.intellij.openapi.editor.colors.TextAttributesKey;

import static com.intellij.openapi.editor.DefaultLanguageHighlighterColors.*;
import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;


public class FitnesseHighlighterColors {

    public static final TextAttributesKey COMMENTS = createTextAttributesKey("FITNESSE_COMMENT", LINE_COMMENT);

    public static final TextAttributesKey SYMBOLS = createTextAttributesKey("FITNESSE_SYMBOL", KEYWORD);

    public static final TextAttributesKey HEADINGS = createTextAttributesKey("FITNESSE_HEADING", METADATA);

    public static final TextAttributesKey COMMANDS = createTextAttributesKey("FITNESSE_COMMAND", KEYWORD);

    public static final TextAttributesKey TABLE_HEADINGS = createTextAttributesKey("FITNESSE_TABLE_HEADING", IDENTIFIER);
}