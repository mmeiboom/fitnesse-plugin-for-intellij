package nl.mmeiboom.fitnesseplugin.highlighter;

import com.intellij.lexer.EmptyLexer;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.EditorColorsManager;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.editor.markup.TextAttributes;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import nl.mmeiboom.fitnesseplugin.lang.FitnesseTokenTypeSets;
import nl.mmeiboom.fitnesseplugin.parser.FitnesseLexerAdapter;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseElementType;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTypes;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class FitnesseSyntaxHighlighter extends SyntaxHighlighterBase {

  protected final Lexer lexer = new EmptyLexer();

  protected static final Map<IElementType, TextAttributesKey> ATTRIBUTES = new HashMap<IElementType, TextAttributesKey>();

  static {
    fillMap(ATTRIBUTES, FitnesseTokenTypeSets.COMMENT_SET, FitnesseHighlighterColors.COMMENTS);
    fillMap(ATTRIBUTES, FitnesseTokenTypeSets.SYMBOL_SET, FitnesseHighlighterColors.SYMBOLS);
    fillMap(ATTRIBUTES, FitnesseTokenTypeSets.HEADING_SET, FitnesseHighlighterColors.HEADINGS);
    fillMap(ATTRIBUTES, FitnesseTokenTypeSets.COMMAND_SET, FitnesseHighlighterColors.COMMANDS);
    fillMap(ATTRIBUTES, FitnesseTokenTypeSets.TABLE_HEADING_SET, FitnesseHighlighterColors.TABLE_HEADINGS);
  }

  @NotNull
  @Override
  public Lexer getHighlightingLexer() {
    return lexer;
    //return new FitnesseLexerAdapter();
  }

  @NotNull
  @Override
  public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
    return pack(ATTRIBUTES.get(tokenType));
  }
}