package nl.mmeiboom.fitnesseplugin.formatter;

import com.intellij.formatting.Alignment;
import com.intellij.formatting.Block;
import com.intellij.formatting.Spacing;
import com.intellij.formatting.SpacingBuilder;
import com.intellij.formatting.Wrap;
import com.intellij.formatting.WrapType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.TokenType;
import com.intellij.psi.formatter.common.AbstractBlock;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTypes;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class FitnesseTableCellBlock extends AbstractBlock {

    private final int cellWidth;
    private final SpacingBuilder spacingBuilder;

    public FitnesseTableCellBlock(ASTNode node, @Nullable Wrap wrap, @Nullable Alignment alignment, SpacingBuilder spacingBuilder, int cellWidth) {
        super(node, wrap, alignment);
        this.spacingBuilder = spacingBuilder;
        this.cellWidth = cellWidth;
    }

    @Override
    protected List<Block> buildChildren() {
        List<Block> blocks = new ArrayList<Block>();
        ASTNode child = myNode.getFirstChildNode();
        ASTNode previousChild = null;

        while (child != null) {
            if (child.getElementType() != TokenType.WHITE_SPACE &&
                    (previousChild == null || previousChild.getElementType() != FitnesseTypes.CRLF ||
                            child.getElementType() != FitnesseTypes.CRLF)) {
                Block block = new FitnesseBlock(child, Wrap.createWrap(WrapType.NONE, false), Alignment.createAlignment(), spacingBuilder);
                blocks.add(block);
            }
            previousChild = child;
            child = child.getTreeNext();
        }
        return blocks;
    }

    @Nullable
    @Override
    public Spacing getSpacing(Block child1, Block child2) {
        return spacingBuilder.getSpacing(this, child1, child2);
    }

    public int getCellWidth() {
        return cellWidth;
    }

    @Override
    public boolean isLeaf() {
        return false;
    }
}
