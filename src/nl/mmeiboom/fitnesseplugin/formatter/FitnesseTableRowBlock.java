package nl.mmeiboom.fitnesseplugin.formatter;

import com.intellij.formatting.Alignment;
import com.intellij.formatting.Block;
import com.intellij.formatting.Spacing;
import com.intellij.formatting.SpacingBuilder;
import com.intellij.formatting.Wrap;
import com.intellij.formatting.WrapType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.TokenType;
import com.intellij.psi.formatter.common.AbstractBlock;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTableRow;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTypes;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class FitnesseTableRowBlock extends AbstractBlock {

    private final int[] cellWidths;
    private final SpacingBuilder spacingBuilder;

    public FitnesseTableRowBlock(ASTNode node, @Nullable Wrap wrap, @Nullable Alignment alignment, SpacingBuilder spacingBuilder, int[] cellWidths) {
        super(node, wrap, alignment);
        this.spacingBuilder = spacingBuilder;
        this.cellWidths = cellWidths;
    }

    @Override
    protected List<Block> buildChildren() {
        List<Block> blocks = new ArrayList<Block>();
        ASTNode child = myNode.getFirstChildNode();
        ASTNode previousChild = null;

        int i = 0;
        int cellWidth;
        int cellCount = ((FitnesseTableRow) myNode.getPsi()).getTableCellList().size();

        while (child != null) {
            if (child.getElementType() == FitnesseTypes.TABLE_CELL) {
                cellWidth = cellWidths[i];
                if (i == cellCount - 1) {
                    // Last cell in row!!!
                    while (i < cellWidths.length - 1) {
                        i++;
                        cellWidth = cellWidth + 1 + cellWidths[i];
                    }
                }

                Block block = new FitnesseTableCellBlock(child, Wrap.createWrap(WrapType.NONE, false), Alignment.createAlignment(), spacingBuilder, cellWidth);
                blocks.add(block);
                i++;
            } else if (child.getElementType() == FitnesseTypes.TABLE_CELL_DELIM) {
                Block block = new FitnesseTableCellDelimBlock(child, Wrap.createWrap(WrapType.NONE, false), Alignment.createAlignment(), spacingBuilder);
                blocks.add(block);
            } else if (child.getElementType() != TokenType.WHITE_SPACE &&
                    (previousChild == null || previousChild.getElementType() != FitnesseTypes.CRLF ||
                            child.getElementType() != FitnesseTypes.CRLF)) {
                Block block = new FitnesseBlock(child, Wrap.createWrap(WrapType.NONE, false), Alignment.createAlignment(), spacingBuilder);
                blocks.add(block);
            }
            previousChild = child;
            child = child.getTreeNext();
        }
        return blocks;
    }

    @Nullable
    @Override
    public Spacing getSpacing(Block child1, Block child2) {

        if (child1 instanceof FitnesseTableCellBlock && child2 instanceof FitnesseTableCellDelimBlock) {
            String text = ((FitnesseTableCellBlock) child1).getNode().getText();

            if (text.startsWith("Another")) {
                System.out.println(child1);
            }
            int textLength = ((FitnesseTableCellBlock) child1).getNode().getTextLength();
            int columnWidth = ((FitnesseTableCellBlock) child1).getCellWidth();
            int spaces = Math.max(0, columnWidth - textLength);
            return Spacing.createSpacing(spaces, spaces, 0, true, 0);
        }

        if (child2 instanceof FitnesseTableCellBlock && child1 instanceof FitnesseTableCellDelimBlock) {
            return Spacing.createSpacing(0, 0, 0, true, 0);
        } else {
            return spacingBuilder.getSpacing(this, child1, child2);
        }
    }

    @Override
    public boolean isLeaf() {
        return false;
    }
}
