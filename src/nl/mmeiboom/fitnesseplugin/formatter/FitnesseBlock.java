package nl.mmeiboom.fitnesseplugin.formatter;

import com.intellij.formatting.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.TokenType;
import com.intellij.psi.formatter.common.AbstractBlock;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTableRow;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTypes;
import nl.mmeiboom.fitnesseplugin.psi.impl.FitnesseTableImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class FitnesseBlock extends AbstractBlock {
    private SpacingBuilder spacingBuilder;

    protected FitnesseBlock(@NotNull ASTNode node, @Nullable Wrap wrap, @Nullable Alignment alignment,
                            SpacingBuilder spacingBuilder) {
        super(node, wrap, alignment);
        this.spacingBuilder = spacingBuilder;
    }

    @Override
    protected List<Block> buildChildren() {
        // Special processing for tables, to align columns vertically
        if (myNode.getElementType() == FitnesseTypes.TABLE) {
            try {
                FitnesseTableImpl psiTable = (FitnesseTableImpl) myNode.getPsi();
                return buildTableBlocks(psiTable);
            } catch (FormattingException e) {
                // Ignore, we'll fallback to regular
            }
        }

        return buildRegularBlocks();
    }

    private List<Block> buildTableBlocks(FitnesseTableImpl psiTable) throws FormattingException {

        List<Block> blocks = new ArrayList<Block>();
        int[] cellWidths = psiTable.getColumnWidths();

        Block block = new FitnesseTableHeaderBlock(psiTable.getHeader().getNode(), Wrap.createWrap(WrapType.NONE, false), Alignment.createAlignment(), spacingBuilder, cellWidths);
        blocks.add(block);

        for (FitnesseTableRow tableRow : psiTable.getTableRowList()) {
            block = new FitnesseTableRowBlock(tableRow.getNode(), Wrap.createWrap(WrapType.NONE, false), Alignment.createAlignment(), spacingBuilder, cellWidths);
            blocks.add(block);
        }
        return blocks;
    }

    private List<Block> buildRegularBlocks() {
        List<Block> blocks = new ArrayList<Block>();
        ASTNode child = myNode.getFirstChildNode();
        ASTNode previousChild = null;

        // Plain processing
        while (child != null) {
            if (child.getElementType() != TokenType.WHITE_SPACE &&
                    (previousChild == null || previousChild.getElementType() != FitnesseTypes.CRLF ||
                            child.getElementType() != FitnesseTypes.CRLF)) {
                Block block = new FitnesseBlock(child, Wrap.createWrap(WrapType.NONE, false), Alignment.createAlignment(),
                        spacingBuilder);
                blocks.add(block);
            }
            previousChild = child;
            child = child.getTreeNext();
        }
        return blocks;
    }


    @Override
    public Indent getIndent() {
        return Indent.getAbsoluteNoneIndent();
    }

    @Nullable
    @Override
    public Spacing getSpacing(@Nullable Block child1, @NotNull Block child2) {
        return null;
    }

    @Override
    public boolean isLeaf() {
        return myNode.getFirstChildNode() == null;
    }
}