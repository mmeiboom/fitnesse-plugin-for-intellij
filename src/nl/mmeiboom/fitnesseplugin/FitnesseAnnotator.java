package nl.mmeiboom.fitnesseplugin;

import com.intellij.lang.annotation.Annotation;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiComment;
import com.intellij.psi.PsiElement;
import nl.mmeiboom.fitnesseplugin.highlighter.FitnesseHighlighterColors;
import nl.mmeiboom.fitnesseplugin.highlighter.FitnesseSyntaxHighlighter;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseCommandBlock;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseCommentHead;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseDecisionHead;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseHeading;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseImportHead;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseLibraryHead;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseQueryHead;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseScenarioHead;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseScenarioName;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseScriptHead;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseSymbol;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseVariable;
import org.jetbrains.annotations.NotNull;

public class FitnesseAnnotator implements Annotator {
  @Override
  public void annotate(@NotNull final PsiElement element, @NotNull AnnotationHolder holder) {

    if (element instanceof FitnesseScenarioName) {
      TextRange range = new TextRange(element.getTextRange().getStartOffset(), element.getTextRange().getEndOffset());
      Annotation annotation = holder.createInfoAnnotation(range, null);
      annotation.setTextAttributes(FitnesseHighlighterColors.HEADINGS);
    }

    // Table Headers
    if ((element instanceof FitnesseDecisionHead) ||
            (element instanceof FitnesseScriptHead) ||
            (element instanceof FitnesseScenarioHead) ||
            (element instanceof FitnesseLibraryHead) ||
            (element instanceof FitnesseImportHead) ||
            (element instanceof FitnesseQueryHead) ||
            (element instanceof FitnesseCommentHead)
    ) {
      TextRange range = new TextRange(element.getTextRange().getStartOffset(), element.getTextRange().getEndOffset());
      Annotation annotation = holder.createInfoAnnotation(range, null);
      annotation.setTextAttributes(FitnesseHighlighterColors.TABLE_HEADINGS);
    }


    if (element instanceof PsiComment) {
      TextRange range = new TextRange(element.getTextRange().getStartOffset(), element.getTextRange().getEndOffset());
      Annotation annotation = holder.createInfoAnnotation(range, null);
      annotation.setTextAttributes(FitnesseHighlighterColors.COMMENTS);
    }

    if (element instanceof FitnesseSymbol) {
      TextRange range = new TextRange(element.getTextRange().getStartOffset(), element.getTextRange().getEndOffset());
      Annotation annotation = holder.createInfoAnnotation(range, null);
      annotation.setTextAttributes(FitnesseHighlighterColors.SYMBOLS);
    }

    if (element instanceof FitnesseVariable) {
      TextRange range = new TextRange(element.getTextRange().getStartOffset(), element.getTextRange().getEndOffset());
      Annotation annotation = holder.createInfoAnnotation(range, null);
      annotation.setTextAttributes(FitnesseHighlighterColors.SYMBOLS);
    }

//    if (element instanceof FitnesseFixtureName) {
//      TextRange range = new TextRange(element.getTextRange().getStartOffset() - 1, element.getTextRange().getEndOffset() + 1);
//      Annotation annotation = holder.createInfoAnnotation(range, null);
//      annotation.setTextAttributes(FitnesseHighlighterColors.SYMBOLS);
//    }

    if (element instanceof FitnesseCommandBlock) {
      TextRange range = new TextRange(element.getTextRange().getStartOffset(), element.getTextRange().getEndOffset());
      Annotation annotation = holder.createInfoAnnotation(range, null);
      annotation.setTextAttributes(FitnesseHighlighterColors.COMMANDS);
    }

    if (element instanceof FitnesseHeading) {
      TextRange range = new TextRange(element.getTextRange().getStartOffset(), element.getTextRange().getEndOffset());
      Annotation annotation = holder.createInfoAnnotation(range, null);
      annotation.setTextAttributes(FitnesseHighlighterColors.HEADINGS);
    }

  }
}