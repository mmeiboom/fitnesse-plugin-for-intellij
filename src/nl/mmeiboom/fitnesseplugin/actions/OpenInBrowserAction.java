/*
 * Copyright 2011-2014 Gregory Shrago
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.mmeiboom.fitnesseplugin.actions;

import com.intellij.ide.BrowserUtil;
import com.intellij.notification.Notification;
import com.intellij.notification.NotificationGroup;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vcs.changes.BackgroundFromStartOption;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.util.ExceptionUtil;

import nl.mmeiboom.fitnesseplugin.settings.FitnesseGlobalSettings;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashSet;
import java.util.Set;


public class OpenInBrowserAction extends AnAction implements DumbAware {

  private static final Logger LOG = Logger.getInstance("org.intellij.grammar.actions.OpenInBrowserAction");

  @Override
  public void actionPerformed(final AnActionEvent actionEvent) {

    // Prerequisites
    FitnesseGlobalSettings settings = FitnesseGlobalSettings.getInstance();
    PsiFile file = actionEvent.getData(LangDataKeys.PSI_FILE);
    StringBuilder sb = new StringBuilder();
    if (file == null) {
      return;
    }

    // Build URL for testcase
    String host = (settings.getFitnesseHost() == null ? "localhost" : settings.getFitnesseHost());
    // ... prefix with default protocol if needed
    if(!host.startsWith("http")) {
      sb.append("http://");
    }
    // ... remove trailing slash
    sb.append(host);

    if (settings.getFitnessePort() > 0) {
      sb.append(":");
      sb.append(settings.getFitnessePort());
    } else {
      sb.append(":8085");
    }

    String path = FileUtil.toSystemDependentName(file.getVirtualFile().getPath());

    String[] parts = StringUtils.split(path, File.separatorChar);
    boolean inRoot = false;
    for (String part : parts) {
      if (inRoot && !part.equals("content.txt")) {
        sb.append(".");
        sb.append(part);
      }
      if (part.equals("FrontPage")) {
        sb.append("/");
        sb.append(part);
        inRoot = true;
      }
    }

    openBrowser(sb.toString());
  }

  protected String getCurrentFileName(AnActionEvent e) {
    PsiFile file = e.getData(LangDataKeys.PSI_FILE);
    return file == null ? "" : file.getName();
  }

  private void openBrowser(String url) {
    try {
      BrowserUtil.browse(url);
    } catch (Exception e) {
      System.err.println("Error: " + e.getMessage());
    }
  }
}