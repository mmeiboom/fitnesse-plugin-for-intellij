package nl.mmeiboom.fitnesseplugin;

import com.intellij.lang.refactoring.RefactoringSupportProvider;
import com.intellij.psi.PsiElement;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseProperty;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseVariable;

public class FitnesseRefactoringSupportProvider extends RefactoringSupportProvider {
    @Override
    public boolean isMemberInplaceRenameAvailable(PsiElement element, PsiElement context) {
        return element instanceof FitnesseVariable;
    }
}