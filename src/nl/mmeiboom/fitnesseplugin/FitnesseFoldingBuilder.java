package nl.mmeiboom.fitnesseplugin;

import com.intellij.lang.ASTNode;
import com.intellij.lang.folding.FoldingBuilderEx;
import com.intellij.lang.folding.FoldingDescriptor;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.FoldingGroup;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiLiteralExpression;
import com.intellij.psi.search.PsiElementProcessor;
import com.intellij.psi.util.PsiTreeUtil;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseCollapsableBlock;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseCollapsableLabel;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseFile;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseProperty;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTypes;
import nl.mmeiboom.fitnesseplugin.psi.impl.FitnesseCollapsableBlockImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FitnesseFoldingBuilder extends FoldingBuilderEx {
  @NotNull
  @Override
  public FoldingDescriptor[] buildFoldRegions(@NotNull PsiElement root, @NotNull Document document, boolean quick) {

    if (!(root instanceof FitnesseFile)) return FoldingDescriptor.EMPTY;
    FitnesseFile file = (FitnesseFile) root;

    final List<FoldingDescriptor> descriptors = new ArrayList<FoldingDescriptor>();

    if (!quick) {
      PsiTreeUtil.processElements(file, new PsiElementProcessor() {
        @Override
        public boolean execute(@NotNull PsiElement element) {
          if (element.getNode().getElementType() == FitnesseTypes.COLLAPSABLE_BLOCK) {
            descriptors.add(new FoldingDescriptor(element, element.getTextRange()));
          }
          return true;
        }
      });
    }

    return descriptors.toArray(new FoldingDescriptor[descriptors.size()]);
  }

  @Nullable
  @Override
  public String getPlaceholderText(@NotNull ASTNode node) {
    PsiElement psi = node.getPsi();
    if (psi instanceof FitnesseCollapsableBlockImpl) {
        FitnesseCollapsableLabel collapsableLabel = ((FitnesseCollapsableBlockImpl) psi).getCollapsableLabel();
        String label = (collapsableLabel == null ? "..." : collapsableLabel.getText());
        return "!* " + label + " *!";
    }
    return null;
  }

  @Override
  public boolean isCollapsedByDefault(@NotNull ASTNode node) {
    return true;
  }
}