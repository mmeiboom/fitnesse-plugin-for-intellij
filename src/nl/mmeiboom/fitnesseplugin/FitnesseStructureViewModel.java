package nl.mmeiboom.fitnesseplugin;

import com.intellij.ide.structureView.StructureViewModel;
import com.intellij.ide.structureView.StructureViewModelBase;
import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.util.treeView.smartTree.Sorter;
import com.intellij.psi.PsiFile;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseFile;
import org.jetbrains.annotations.NotNull;

public class FitnesseStructureViewModel extends StructureViewModelBase implements
        StructureViewModel.ElementInfoProvider {
    public FitnesseStructureViewModel(PsiFile psiFile) {
        super(psiFile, new FitnesseStructureViewElement(psiFile));
    }

    @NotNull
    public Sorter[] getSorters() {
        return new Sorter[] {Sorter.ALPHA_SORTER};
    }


    @Override
    public boolean isAlwaysShowsPlus(StructureViewTreeElement element) {
        return false;
    }

    @Override
    public boolean isAlwaysLeaf(StructureViewTreeElement element) {
        return element instanceof FitnesseFile;
    }
}