package nl.mmeiboom.fitnesseplugin.settings;

import javax.swing.*;

/**
 * UI form for FitnesseGlobalSettings
 */
public class FitnesseSettingsPanel {

  /**
   * The parent panel for the form.
   */
  public JPanel panel;

  /**
   * The "settings" form container.
   */
  public JPanel settingsPanel;

  public JTextField fitnessePortText;
  public JLabel fitnessePortLabel;
  public JTextField fitnesseHostText;
  private JLabel fitnesseHostLabel;

}