package nl.mmeiboom.fitnesseplugin.settings;

import org.jetbrains.annotations.NotNull;

import java.util.EventListener;

public interface FitnesseGlobalSettingsListener extends EventListener {

    void handleSettingsChanged(@NotNull final FitnesseGlobalSettings newSettings);
}