package nl.mmeiboom.fitnesseplugin.settings;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import org.jdom.Element;
import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Set;

/**
 * Persistent global settings object for the Fitnesse plugin.
 *
 * @see <a href="http://confluence.jetbrains.com/display/IDEADEV/Persisting+State+of+Components">IDEA DEV - Persisting state</a>
 */
@State(
        name = "FitnesseSettings",
        storages = @Storage(id = "other", file = "$APP_CONFIG$/fitnesse.xml")
)
public class FitnesseGlobalSettings implements PersistentStateComponent<Element> {

  protected Set<WeakReference<FitnesseGlobalSettingsListener>> listeners;

  private int parsingTimeout = 10000;
  private String fitnesseHost = "localhost";
  private int fitnessePort = 8085;

  public static FitnesseGlobalSettings getInstance() {
    return ServiceManager.getService(FitnesseGlobalSettings.class);
  }

  public int getParsingTimeout() {
    return parsingTimeout;
  }

  public void setParsingTimeout(int parsingTimeout) {
    if (this.parsingTimeout != parsingTimeout) {
      this.parsingTimeout = parsingTimeout;
      notifyListeners();
    }
  }

  public Element getState() {
    final Element element = new Element("FitnesseSettings");
    element.setAttribute("fitnesseHost", fitnesseHost);
    element.setAttribute("fitnessePort", Integer.toString(fitnessePort));
    return element;
  }

  public void loadState(@NotNull Element element) {

    String value = element.getAttributeValue("fitnesseHost");
    if (value != null) fitnesseHost = value;

    value = element.getAttributeValue("fitnessePort");
    if (value != null) fitnessePort = Integer.parseInt(value);

    notifyListeners();
  }

  public void addListener(@NotNull final FitnesseGlobalSettingsListener listener) {
    if (listeners == null) listeners = new HashSet<WeakReference<FitnesseGlobalSettingsListener>>();
    listeners.add(new WeakReference<FitnesseGlobalSettingsListener>(listener));
  }

  protected void notifyListeners() {
    if (listeners != null)
      for (final WeakReference<FitnesseGlobalSettingsListener> listenerRef : listeners)
        if (listenerRef.get() != null) listenerRef.get().handleSettingsChanged(this);
  }

  public String getFitnesseHost() {
    return fitnesseHost;
  }

  public void setFitnesseHost(String fitnesseHost) {
    this.fitnesseHost = fitnesseHost;
  }

  public int getFitnessePort() {
    return fitnessePort;
  }

  public void setFitnessePort(int fitnessePort) {
    this.fitnessePort = fitnessePort;
  }
}