package nl.mmeiboom.fitnesseplugin.settings;

import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.intellij.psi.codeStyle.CustomCodeStyleSettings;

public class FitnesseCodeStyleSettings extends CustomCodeStyleSettings {
    public FitnesseCodeStyleSettings(CodeStyleSettings settings) {
        super("FitnesseCodeStyleSettings", settings);
    }
}