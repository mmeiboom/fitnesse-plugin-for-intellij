package nl.mmeiboom.fitnesseplugin.settings;

import com.intellij.openapi.options.SearchableConfigurable;
import nl.mmeiboom.fitnesseplugin.lang.FitnesseLanguage;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.Icon;
import javax.swing.JComponent;

public class FitnesseGlobalSettingsConfigurable implements SearchableConfigurable {

  /**
   * The settings storage object.
   */
  protected FitnesseGlobalSettings globalSettings;

  /**
   * The settings UI form.
   */
  protected FitnesseSettingsPanel settingsPanel;

  public FitnesseGlobalSettingsConfigurable() {
    globalSettings = FitnesseGlobalSettings.getInstance();
  }

  public JComponent createComponent() {
    if (settingsPanel == null) settingsPanel = new FitnesseSettingsPanel();
    reset();
    return settingsPanel.panel;
  }

  public boolean isModified() {
    return settingsPanel == null
            || settingsPanel.fitnesseHostText == null || !settingsPanel.fitnesseHostText.getText().equals(globalSettings.getFitnesseHost())
            || settingsPanel.fitnessePortText == null || !settingsPanel.fitnessePortText.getText().equals(globalSettings.getFitnessePort());
  }

  public void apply() {
    if (settingsPanel != null) {
      globalSettings.setFitnesseHost(settingsPanel.fitnesseHostText.getText());
      globalSettings.setFitnessePort(Integer.parseInt(settingsPanel.fitnessePortText.getText()));
    }
  }

  public void reset() {
    if (globalSettings != null && settingsPanel != null) {
      if (settingsPanel.fitnesseHostText != null) settingsPanel.fitnesseHostText.setText(globalSettings.getFitnesseHost());
      if (settingsPanel.fitnessePortText != null) settingsPanel.fitnessePortText.setText(String.valueOf(globalSettings.getFitnessePort()));
    }
  }

  public String getHelpTopic() {
    return getId();
  }

  @Override
  public void disposeUIResources() {
    settingsPanel = null;
  }

  public Icon getIcon() {
    return null;
  }

  @Nls
  public String getDisplayName() {
    return getId();
  }

  @NotNull
  public String getId() {
    return FitnesseLanguage.INSTANCE.getDisplayName();
  }

  @Nullable
  @Override
  public Runnable enableSearch(String option) {
    return null;
  }

}