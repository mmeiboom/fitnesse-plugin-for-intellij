package nl.mmeiboom.fitnesseplugin.settings;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import nl.mmeiboom.fitnesseplugin.FitnesseIcons;
import nl.mmeiboom.fitnesseplugin.highlighter.FitnesseHighlighterColors;
import nl.mmeiboom.fitnesseplugin.highlighter.FitnesseSyntaxHighlighter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Map;

public class FitnesseColorSettingsPage implements ColorSettingsPage {
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
            new AttributesDescriptor("Symbols and Variables", FitnesseHighlighterColors.SYMBOLS),
            new AttributesDescriptor("Comments", FitnesseHighlighterColors.COMMENTS),
            new AttributesDescriptor("Headings", FitnesseHighlighterColors.HEADINGS),
            new AttributesDescriptor("Commands", FitnesseHighlighterColors.COMMANDS),
            new AttributesDescriptor("Tables", FitnesseHighlighterColors.TABLE_HEADINGS)
            //new AttributesDescriptor("Bad character", FitnesseHighlighterColors.BAD_CHARACTER)
    };

    @Nullable
    @Override
    public Icon getIcon() {
        return FitnesseIcons.FILE;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new FitnesseSyntaxHighlighter();
    }

    @NotNull
    @Override
    public String getDemoText() {
        return "|scenario|scenario name with three parameters, _, _ and _|a,b,c|\n|cell1|cell2|cell3|cell with a $symbol|wide cell|\n\nJust some text with a ${variable}.\n\n!*> Collapsed section\nSection content\n*!\n\n";
    }

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "FitNesse";
    }
}