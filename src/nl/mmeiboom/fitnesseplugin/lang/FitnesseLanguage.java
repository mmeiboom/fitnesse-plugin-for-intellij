package nl.mmeiboom.fitnesseplugin.lang;

import com.intellij.lang.Language;

public class FitnesseLanguage extends Language {
    public static final FitnesseLanguage INSTANCE = new FitnesseLanguage();

    private FitnesseLanguage() {
        super("FitNesse");
    }
}