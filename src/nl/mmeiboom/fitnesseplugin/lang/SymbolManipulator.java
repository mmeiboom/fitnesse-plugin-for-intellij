package nl.mmeiboom.fitnesseplugin.lang;

import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.AbstractElementManipulator;
import com.intellij.util.IncorrectOperationException;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseSymbol;

public class SymbolManipulator extends AbstractElementManipulator<FitnesseSymbol> {
  @Override
  public FitnesseSymbol handleContentChange(FitnesseSymbol element, TextRange range, String newContent) throws IncorrectOperationException {
    TextRange valueRange = getRangeInElement(element);
    final String oldText = element.getText();
    String newText = oldText.substring(0, range.getStartOffset()) + newContent + oldText.substring(range.getEndOffset());
    element.setName(newText.substring(valueRange.getStartOffset()).replaceAll("([^\\s])\n", "$1 \n"));  // add explicit space before \n
    return element;
  }
  @Override
  public TextRange getRangeInElement(FitnesseSymbol element) {
    ASTNode valueNode = element.getNode().getLastChildNode();
    if (valueNode == null) return TextRange.from(element.getTextLength(), 0);
    TextRange range = valueNode.getTextRange();
    return TextRange.from(range.getStartOffset() - element.getTextRange().getStartOffset(), range.getLength());
  }
}