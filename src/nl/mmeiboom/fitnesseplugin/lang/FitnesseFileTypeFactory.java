package nl.mmeiboom.fitnesseplugin.lang;

import com.intellij.openapi.fileTypes.FileNameMatcher;
import com.intellij.openapi.fileTypes.FileTypeConsumer;
import com.intellij.openapi.fileTypes.FileTypeFactory;
import com.intellij.openapi.fileTypes.WildcardFileNameMatcher;
import org.jetbrains.annotations.NotNull;

public class FitnesseFileTypeFactory extends FileTypeFactory {
  @Override
  public void createFileTypes(@NotNull FileTypeConsumer fileTypeConsumer) {
    FileNameMatcher matcher = new WildcardFileNameMatcher("content.txt");
    fileTypeConsumer.consume(FitnesseFileType.INSTANCE, matcher);
  }
}