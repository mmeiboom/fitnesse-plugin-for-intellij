package nl.mmeiboom.fitnesseplugin.lang;

import com.intellij.openapi.fileTypes.LanguageFileType;
import nl.mmeiboom.fitnesseplugin.FitnesseIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class FitnesseFileType extends LanguageFileType {
    public static final FitnesseFileType INSTANCE = new FitnesseFileType();

    private FitnesseFileType() {
        super(FitnesseLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "FitNesse Wiki file";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "FitNesse Wiki file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "txt";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return FitnesseIcons.FILE;
    }
}