package nl.mmeiboom.fitnesseplugin.lang;

import com.intellij.psi.tree.TokenSet;
import nl.mmeiboom.fitnesseplugin.psi.FitnesseTypes;


public interface FitnesseTokenTypeSets extends FitnesseTypes {

    TokenSet COMMENT_SET = TokenSet.create(COMMENT);

    TokenSet SYMBOL_SET = TokenSet.create(SYMBOL, VARIABLE);

    TokenSet HEADING_SET = TokenSet.create(HEADING);

    TokenSet TABLE_HEADING_SET = TokenSet.create(TABLE_START);

    TokenSet COMMAND_SET = TokenSet.create(COMMAND_BLOCK);
}